/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create character class. Allows parsing of a file string.
*/
#pragma once

using namespace std;

class Chars {

private:
	string characters;
	int currentIndex;
	bool eof(unsigned int index) const;
public:
	Chars(string inputCharacters);
	~Chars();
	bool eof() const;
	char currentChar();
	void increment();
	void decrement();
};