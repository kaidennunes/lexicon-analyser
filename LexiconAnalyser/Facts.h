/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/

#pragma once

#include "Fact.h"
#include "Lexer.h"
#include "String.h"
#include "Id.h"
#include "StringList.h"
#include <algorithm>

using namespace std;

class Facts
{
private:
	vector<Fact> facts;
	vector<string> domain;
public:
	Facts();
	~Facts();
	void parseScheme(string expectedToken, Lexer *lexer);
	void printDomain();
	vector<Fact> getFacts();
	friend ostream& operator <<(ostream &os, Facts &Facts);
};

