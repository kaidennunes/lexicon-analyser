/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Define lexer class. Prints tokens. Parses tokens
*/
#include "stdafx.h"
#include "DatalogParser.h"

DatalogParser::DatalogParser()
{
}

DatalogParser::~DatalogParser()
{
}

void DatalogParser::parseTokens(Lexer lexer)
{
	schemes.parseScheme("SCHEMES", &lexer);
	facts.parseScheme("FACTS", &lexer);
	rules.parseScheme("RULES", &lexer);
	queries.parseScheme("QUERIES", &lexer);
}

vector<Scheme> DatalogParser::getSchemes()
{
	return schemes.getSchemes();
}

vector<Fact> DatalogParser::getFacts()
{
	return facts.getFacts();
}

vector<Rule> DatalogParser::getRules()
{
	return rules.getRules();
}

vector<Query> DatalogParser::getQueries()
{
	return queries.getQueries();
}

void DatalogParser::printData()
{
	cout << queries;
}