/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/

#include "stdafx.h"
#include "Rules.h"


Rules::Rules()
{
}


Rules::~Rules()
{
}

void Rules::parseScheme(string expectedToken, Lexer *lexer)
{
	// Get current token (should be a RULES token)
	Token currentToken = lexer->getCurrentToken(expectedToken);

	// We now expect a colon
	expectedToken = "COLON";
	currentToken = lexer->getCurrentToken(expectedToken);

	// Parse through all the tokens until we finishing the rules section
	while (lexer->getNumberOfTokens() > 0 && lexer->peekAtCurrentToken().getTokenType() != "QUERIES")
	{
		// We now expect rules (head predicate first)
		Id firstId = Id(lexer);

		expectedToken = "LEFT_PAREN";
		currentToken = lexer->getCurrentToken(expectedToken);

		Id secondId = Id(lexer);

		IdList idList = IdList();

		idList.addId(secondId);

		// Get list of id's
		while (lexer->peekAtCurrentToken().getTokenType() == "COMMA")
		{
			expectedToken = "COMMA";
			currentToken = lexer->getCurrentToken(expectedToken);

			Id tempString = Id(lexer);

			idList.addId(tempString);
		}

		expectedToken = "RIGHT_PAREN";
		currentToken = lexer->getCurrentToken(expectedToken);
		HeadPredicate headPredicate = HeadPredicate(firstId, idList);

		// End of head predicate

		expectedToken = "COLON_DASH";
		currentToken = lexer->getCurrentToken(expectedToken);

		// Predicate
		Id firstPredicateId = Id(lexer);

		expectedToken = "LEFT_PAREN";
		currentToken = lexer->getCurrentToken(expectedToken);

		// Some parameter 
		Parameter firstParameter = parseParameter(expectedToken, currentToken, lexer);

		// Parameter list
		ParameterList firstParameterList = ParameterList();

		firstParameterList.addParameter(firstParameter);

		while (lexer->peekAtCurrentToken().getTokenType() == "COMMA")
		{
			expectedToken = "COMMA";
			currentToken = lexer->getCurrentToken(expectedToken);

			Parameter tempParameter = parseParameter(expectedToken, currentToken, lexer);

			firstParameterList.addParameter(tempParameter);
		}

		expectedToken = "RIGHT_PAREN";
		currentToken = lexer->getCurrentToken(expectedToken);

		Predicate predicate = Predicate(firstPredicateId, firstParameterList);

		PredicateList predicateList = PredicateList();
		predicateList.addPredicate(predicate);

		// Predicate List
		while (lexer->peekAtCurrentToken().getTokenType() == "COMMA")
		{
			expectedToken = "COMMA";
			currentToken = lexer->getCurrentToken(expectedToken);

			Id firstListPredicateId = Id(lexer);

			expectedToken = "LEFT_PAREN";
			currentToken = lexer->getCurrentToken(expectedToken);

			// Some parameter 
			Parameter secondListPredicateParameter = parseParameter(expectedToken, currentToken, lexer);

			// Parameter list
			ParameterList firstListParameterList = ParameterList();

			firstListParameterList.addParameter(secondListPredicateParameter);

			while (lexer->peekAtCurrentToken().getTokenType() == "COMMA")
			{
				expectedToken = "COMMA";
				currentToken = lexer->getCurrentToken(expectedToken);

				Parameter tempParameter = parseParameter(expectedToken, currentToken, lexer);

				firstListParameterList.addParameter(tempParameter);
			}

			expectedToken = "RIGHT_PAREN";
			currentToken = lexer->getCurrentToken(expectedToken);

			Predicate predicate = Predicate(firstListPredicateId, firstListParameterList);

			predicateList.addPredicate(predicate);
		}

		expectedToken = "PERIOD";
		currentToken = lexer->getCurrentToken(expectedToken);

		Rule newRule = Rule(headPredicate, predicateList);
		rules.push_back(newRule);
	}
}


Parameter Rules::parseParameter(string expectedToken, Token currentToken, Lexer *lexer)
{
	// Must be a STRING or ID or expression (the beginning is a LEFT_PAREN )
	string tokenType = lexer->peekAtCurrentToken().getTokenType();
	if (tokenType == "STRING")
	{
		return Parameter(String(lexer));
	}
	else if (tokenType == "ID")
	{
		return Parameter(Id(lexer));
	}
	else
	{
		expectedToken = "LEFT_PAREN";
		Expression * newExpression = parseExpression(expectedToken, currentToken, lexer);
		Parameter newParameter = Parameter(newExpression);
		return newParameter;
	}
	return Parameter();
}

Expression * Rules::parseExpression(string expectedToken, Token currentToken, Lexer *lexer)
{

	// LEFT_PAREN
	currentToken = lexer->getCurrentToken(expectedToken);

	// Get parameter
	Parameter firstParameter = parseParameter(expectedToken, currentToken, lexer);

	string tokenType = lexer->peekAtCurrentToken().getTokenType();
	// Get operator (either ADD or MULTIPLY)
	if (tokenType == "ADD")
	{
		expectedToken = "ADD";
	}
	else
	{
		expectedToken = "MULTIPLY";
	}
	currentToken = lexer->getCurrentToken(expectedToken);
	string newOperator = currentToken.getString();

	Parameter secondParameter = parseParameter(expectedToken, currentToken, lexer);
	// RIGHT_PAREN
	expectedToken = "RIGHT_PAREN";
	currentToken = lexer->getCurrentToken(expectedToken);

	return new Expression(firstParameter, newOperator[0], secondParameter);
}

vector<Rule> Rules::getRules()
{
	return rules;
}

ostream & operator<<(std::ostream & os, Rules & Rules) {
	// Print out header with number of rules
	os << "Rules(" << Rules.rules.size() << "):" << endl;
	// Print out the rules
	for (unsigned int i = 0; i < Rules.rules.size(); i++)
	{
		os << "  ";
		os << Rules.rules[i];
		os << endl;
	}
	return os;
}
