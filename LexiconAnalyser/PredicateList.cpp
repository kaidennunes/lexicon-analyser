#include "stdafx.h"
#include "PredicateList.h"

PredicateList::PredicateList()
{
}


PredicateList::~PredicateList()
{
}

void PredicateList::addPredicate(Predicate predicate)
{
	predicates.push_back(predicate);
}

vector<Predicate> PredicateList::getPredicates()
{
	return predicates;
}

ostream & operator<<(std::ostream & os, PredicateList & PredicateList) {
	// Print out the schemes
	for (unsigned int i = 0; i < PredicateList.predicates.size(); i++)
	{
		os << ',';
		os << PredicateList.predicates[i];
	}
	return os;
}
