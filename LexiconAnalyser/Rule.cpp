/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/

#include "stdafx.h"
#include "Rule.h"


Rule::Rule()
{
}

Rule::Rule(HeadPredicate newHeadPredicate, PredicateList newPredicateList)
{
	headPredicate = newHeadPredicate;
	predicateList = newPredicateList;
	COLON_DASH = ":-";
	PERIOD = '.';
	isTrivial = true;
}

Rule::~Rule()
{
}

HeadPredicate Rule::getHeadPredicate()
{
	return headPredicate;
}

PredicateList Rule::getPredicateList()
{
	return predicateList;
}

bool Rule::getIsTrivial()
{
	return isTrivial;
}

void Rule::setTrivial(bool trivial)
{
	isTrivial = trivial;
}

ostream & operator<<(std::ostream & os, Rule & Rule) {
	os << Rule.headPredicate;
	os << ' ';
	os << Rule.COLON_DASH;
	os << ' ';
	os << Rule.predicateList;
	os << Rule.PERIOD;
	return os;
}