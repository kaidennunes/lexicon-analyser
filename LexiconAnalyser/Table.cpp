/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/
#include "stdafx.h"
#include "Table.h"


Table::Table()
{
}

Table::Table(string newName, Headers newHeaders)
{
	name = newName;
	headers = newHeaders;
}

Table::Table(string newName, Headers newHeaders, set<Row> newRows)
{
	name = newName;
	headers = newHeaders;
	rows = newRows;
}

Table::~Table()
{
}

void Table::addRow(Row newRow)
{
	rows.insert(newRow);
}

// Create a new table with the tuples that match from the column parameters
Table Table::select(ColumnColumnParameters columnColumnParameters)
{
	Table newTable = Table(name, headers);
	for (auto row : rows)
	{
		if (row.indexesHaveSameValue(columnColumnParameters.getFirstColumn(), columnColumnParameters.getSecondColumn()))
		{
			newTable.addRow(row);
		}
	}
	return newTable;
}

// Create a new table with the tuples that match from the column parameters
Table Table::select(ColumnValueParameters columnValueParameters)
{
	Table newTable = Table(name, headers);
	for (auto row : rows)
	{
		if (row.hasValueAtIndex(columnValueParameters.getSecondColumn(), columnValueParameters.getFirstColumn()))
		{
			newTable.addRow(row);
		}
	}
	return newTable;
}

// Create new table with selected columns (you can duplicate the same column)
Table Table::project(vector<int> columnIndexes)
{
	// Create new headers
	Headers newHeader = headers.modifyHeaders(columnIndexes);

	// Create table
	Table newTable = Table(name, newHeader);

	// Add rows to table
	for (auto row : rows)
	{
		Row newRow = row.modifyRows(columnIndexes);
		newTable.addRow(newRow);
	}
	return newTable;
}

// Create new table with new headers
Table Table::rename(Headers newHeader)
{
	return Table(name, newHeader, rows);
}

Table Table::naturalJoin(Table joiningTable)
{
	// Get vector of columns to merge (from merging table)
	vector<int> columnsToMerge = headers.getColumnsToMerge(joiningTable.headers);

	// Merge headers into a new header
	Headers newHeader = headers.mergeWith(joiningTable.headers, columnsToMerge);

	// Initialize new table with the new header
	Table newTable = Table(name, newHeader);

	// Get column mappings (column column pairs)
	set<ColumnColumnParameters> columnMappings = headers.getHeaderMappings(joiningTable.headers);

	// Look at every row in the first table with every row in the second table
	for (auto row : rows)
	{
		for (auto joiningRow : joiningTable.rows)
		{
			// If one row matches another row, then merge the rows and add it to the new table
			if (row.matchs(joiningRow, columnMappings))
			{
				newTable.addRow(row.mergeWith(joiningRow,columnsToMerge));
			}
		}
	}
	return newTable;
}

void Table::tableUnion(Table unionTable)
{
	// Create mapping for headers
	vector<int> mapping = headers.createMappingFor(unionTable.headers);

	// Go through each row in the union table
	for (auto row : unionTable.rows)
	{
		// Rearrange each row according to the mapping
		Row rearrangedRow = row.rearrange(mapping);

		// Add the row to the first table
		rows.insert(rearrangedRow);
	}
}

void Table::printTable(bool hasVariables)
{
	if (rows.size() == 0)
	{
		cout << "No";
		cout << endl;
		return;
	}

	cout << "Yes(";
	cout << rows.size();
	cout << ")";
	cout << endl;

	if (!hasVariables) return;

	for (auto row : rows)
	{
		cout << "  ";
		row.printRow(headers.getHeaders());
		cout << endl;
	}
}

Headers Table::getHeaders()
{
	return headers;
}

int Table::numberOfRows()
{
	return rows.size();
}

ostream & operator<<(std::ostream & os, Table & Table)
{
	os << "Table Name: ";
	os << Table.name;
	os << endl;
	os << Table.headers;
	os << endl;

	for (auto row : Table.rows)
	{
		os << row;
		os << endl;
	}
	return os;
}