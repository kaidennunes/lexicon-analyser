#pragma once

using namespace std;

#include "Lexer.h"

class String
{
private:
	string characters;
public:
	String();
	String(Lexer *lexer);
	~String();
	string getString();
	friend ostream& operator <<(ostream &os, String &String);
};

