/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/

#include "stdafx.h"
#include "Schemes.h"

Schemes::Schemes()
{
}


Schemes::~Schemes()
{
}

// Expected format: ID LEFT_PAREN ID idList RIGHT_PAREN
void Schemes::parseScheme(string expectedToken, Lexer *lexer)
{
	// Get current token (should be a SCHEMES token)
	Token currentToken = lexer->getCurrentToken(expectedToken);

	// We now expect a colon
	expectedToken = "COLON";
	currentToken = lexer->getCurrentToken(expectedToken);

	// Parse through all the tokens until we finishing the schemes section
	do
	{
		// We now expect schemes (ID first)

		Id firstId = Id(lexer);

		expectedToken = "LEFT_PAREN";
		currentToken = lexer->getCurrentToken(expectedToken);

		Id secondId = Id(lexer);

		IdList idList = IdList();

		idList.addId(secondId);

		while (lexer->peekAtCurrentToken().getTokenType() == "COMMA")
		{
			expectedToken = "COMMA";
			currentToken = lexer->getCurrentToken(expectedToken);

			Id tempId = Id(lexer);

			idList.addId(tempId);
		}

		expectedToken = "RIGHT_PAREN";
		currentToken = lexer->getCurrentToken(expectedToken);

		Scheme newScheme = Scheme(firstId, idList);
		schemes.push_back(newScheme);
	} while (lexer->getNumberOfTokens() > 0 && lexer->peekAtCurrentToken().getTokenType() != "FACTS");

}

vector<Scheme> Schemes::getSchemes()
{
	return schemes;
}

ostream & operator<<(std::ostream & os, Schemes & Schemes) {
	// Print out header with number of schemes
	os << "Schemes(" << Schemes.schemes.size() << "):" << endl;
	// Print out the schemes
	for (unsigned int i = 0; i < Schemes.schemes.size(); i++)
	{
		os << "  ";
		os << Schemes.schemes[i];
		os << endl;
	}
	return os;
}
