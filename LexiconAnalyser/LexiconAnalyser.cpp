/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Defines the entry point for application.
Takes command line input, opens a file, and parses the file into tokens, printing it out.
*/
// LexiconAnalyser.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Token.h"
#include "Lexer.h"
#include "DatalogParser.h"
#include "RelationalDatabase.h"
#include "Chars.h"
#include <fstream>

int main(int argc, char *argv[])
{
	// Get input from command line
	string fileLocation("testcase10.txt");

	// Try to open file
	ifstream file;
	file.open(fileLocation);

	// If the file is not valid, we will throw an error and quit
	if (!(file.is_open()))
	{
		return 0;
	}

	// If not, we will read the input into the Chars class
	string content((istreambuf_iterator<char>(file)),
		(istreambuf_iterator<char>()));
	Chars inputStream(content);

	// Then we close the file
	file.close();

	// Create instance of lexer class
	Lexer lexerInterface;

	// Parse input into tokens
	lexerInterface.parseIntoTokens(inputStream);

	// Create instance of datalog parser class
	DatalogParser datalogInterface;
	// Try to parse the tokens.
	// If successful, execute queries
	try
	{
		// Parse tokens
		datalogInterface.parseTokens(lexerInterface);
		// Create instance of relational database class
		RelationalDatabase relationDatabaseInterface(datalogInterface);
		// Execute the queries
		relationDatabaseInterface.executeQueries();
		// Run test cases
		//relationDatabaseInterface.runTestCases();
	}
	// If not, print failure and the offending token
	catch (Token errorToken)
	{
		cout << "Failure!" << endl;
		cout << "  ";
		cout << errorToken;
	}
	return 0;
}

