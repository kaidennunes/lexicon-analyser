/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/

#pragma once

using namespace std;

#include "Predicate.h"
#include "PredicateList.h"

class Query
{
private:
	Predicate predicate;
	char Q_MARK;
public:
	Query();
	Query(Predicate newPredicate);
	~Query();
	Predicate getPredicate();
	friend ostream& operator <<(ostream &os, Query &Query);
};

