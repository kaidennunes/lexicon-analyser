/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create character class. Allows parsing of a file string.
*/
#pragma once

#include "AdjacencyGraph.h"
#include <stack>

class DFSForest
{
private:
	vector<vector<int>> forest = vector<vector<int>>();
	vector<int> iterateThroughMap(vector<int> &ruleDependencies, map<int, bool> &settledNodes, pair<int, set<int>> rule, map<int, set<int>> graph);
public:
	DFSForest();
	DFSForest(AdjacencyGraph mustBeExecutedBeforeGraph);
	DFSForest(AdjacencyGraph dependencyGraph, vector<int> topologicallySortedForest);
	~DFSForest();
	vector<int> generateTopologicallySortedForest();
	vector<vector<int>> createStronglyConnectedComponents();
};