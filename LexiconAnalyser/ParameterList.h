#pragma once

using namespace std;

#include "Parameter.h"

class ParameterList
{
private:
	vector<Parameter> parameters;
public:
	ParameterList();
	~ParameterList();
	void addParameter(Parameter parameter);
	vector<Parameter> getParameters();
	friend ostream& operator <<(ostream &os, ParameterList &ParameterList);
};

