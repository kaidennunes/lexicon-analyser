#include "stdafx.h"
#include "ParameterList.h"


ParameterList::ParameterList()
{
}


ParameterList::~ParameterList()
{
}


void ParameterList::addParameter(Parameter parameter)
{
	parameters.push_back(parameter);
}

vector<Parameter> ParameterList::getParameters()
{
	return parameters;
}

ostream & operator<<(std::ostream & os, ParameterList & ParameterList) {
	// Print out the schemes
	for (unsigned int i = 0; i < ParameterList.parameters.size(); i++)
	{
		if(i != 0) os << ',';
		os << ParameterList.parameters[i];
	}
	return os;
}
