/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/

#pragma once

#include "HeadPredicate.h"
#include "Predicate.h"
#include "PredicateList.h"

using namespace std;

class Rule
{
private:
	HeadPredicate headPredicate;
	PredicateList predicateList;
	string COLON_DASH;
	char PERIOD;
	bool isTrivial;
public:
	Rule();
	Rule(HeadPredicate newHeadPredicate, PredicateList newPredicateList);
	~Rule();
	HeadPredicate getHeadPredicate();
	PredicateList getPredicateList();
	bool getIsTrivial();
	void setTrivial(bool trivial);
	friend ostream& operator <<(ostream &os, Rule &Rule);
};

