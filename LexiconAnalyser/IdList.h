#pragma once

using namespace std;

#include "Id.h"

class IdList
{
private:
	vector<Id> ids;
public:
	IdList();
	~IdList();
	void addId(Id id);
	vector<Id> getIds();
	friend ostream& operator <<(ostream &os, IdList &IdList);
};

