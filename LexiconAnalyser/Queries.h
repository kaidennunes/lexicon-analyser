/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/

#pragma once

#include "Query.h"
#include "Lexer.h"
#include "HeadPredicate.h"
#include "Predicate.h"
#include "PredicateList.h"
#include "IdList.h"
#include "Parameter.h"
#include "Expression.h"

using namespace std;

class Queries
{
private:
	vector<Query> queries;
	Parameter parseParameter(string expectedToken, Token currentToken, Lexer *lexer);
	Expression * parseExpression(string expectedToken, Token currentToken, Lexer *lexer);
public:
	Queries();
	~Queries();
	void parseScheme(string expectedToken, Lexer *lexer);
	vector<Query> getQueries();
	friend ostream& operator <<(ostream &os, Queries &Queries);
};

