#pragma once

#include <vector>
#include <set>
#include "Row.h"
#include "Headers.h"
#include "ColumnColumnParameters.h"
#include "ColumnValueParameters.h"

using namespace std;

class Table
{
private:
	string name;
	Headers headers;
	set<Row> rows;
public:
	Table();
	Table(string newName, Headers newHeaders);
	Table(string newName, Headers newHeaders, set<Row> newRows);
	~Table();
	void addRow(Row newRow);
	Table select(ColumnColumnParameters columnColumnParameters);
	Table select(ColumnValueParameters columnValueParameters);
	Table project(vector<int> columnIndexes);
	Table rename(Headers newHeader);
	Table naturalJoin(Table joiningTable);
	void tableUnion(Table unionTable);
	void printTable(bool hasVariables);
	Headers getHeaders();
	int numberOfRows();
	friend ostream& operator <<(ostream &os, Table &Table);
};

