#include "stdafx.h"
#include "IdList.h"


IdList::IdList()
{
}


IdList::~IdList()
{
}

void IdList::addId(Id id)
{
	ids.push_back(id);
}

vector<Id> IdList::getIds()
{
	return ids;
}

ostream & operator<<(std::ostream & os, IdList & IdList)
{
	// Print out the schemes
	for (unsigned int i = 0; i < IdList.ids.size(); i++)
	{
		os << ',';
		os << IdList.ids[i];
	}
	return os;
}
