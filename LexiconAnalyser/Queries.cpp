/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/

#include "stdafx.h"
#include "Queries.h"


Queries::Queries()
{
}


Queries::~Queries()
{
}

void Queries::parseScheme(string expectedToken, Lexer *lexer)
{
	// Get current token (should be a RULES token)
	Token currentToken = lexer->getCurrentToken(expectedToken);

	// We now expect a colon
	expectedToken = "COLON";
	currentToken = lexer->getCurrentToken(expectedToken);

	// Parse through all the tokens until we finishing the queries section
	do
	{
		// Predicate
		Id firstPredicateId = Id(lexer);

		expectedToken = "LEFT_PAREN";
		currentToken = lexer->getCurrentToken(expectedToken);

		// Some parameter 
		Parameter firstParameter = parseParameter(expectedToken, currentToken, lexer);

		// Parameter list
		ParameterList firstParameterList = ParameterList();

		firstParameterList.addParameter(firstParameter);

		while (lexer->peekAtCurrentToken().getTokenType() == "COMMA")
		{
			expectedToken = "COMMA";
			currentToken = lexer->getCurrentToken(expectedToken);

			Parameter tempParameter = parseParameter(expectedToken, currentToken, lexer);

			firstParameterList.addParameter(tempParameter);
		}

		expectedToken = "RIGHT_PAREN";
		currentToken = lexer->getCurrentToken(expectedToken);

		Predicate predicate = Predicate(firstPredicateId, firstParameterList);

		expectedToken = "Q_MARK";
		currentToken = lexer->getCurrentToken(expectedToken);

		Query newQuery = Query(predicate);
		queries.push_back(newQuery);
	} while (lexer->getNumberOfTokens() > 0 && lexer->peekAtCurrentToken().getTokenType() != "EOF");
}

Parameter Queries::parseParameter(string expectedToken, Token currentToken, Lexer *lexer)
{
	// Must be a STRING or ID or expression (the beginning is a LEFT_PAREN )
	string tokenType = lexer->peekAtCurrentToken().getTokenType();
	if (tokenType == "STRING")
	{
		return Parameter(String(lexer));
	}
	else if (tokenType == "ID")
	{
		return Parameter(Id(lexer));
	}
	else
	{
		expectedToken = "LEFT_PAREN";
		Expression * newExpression = parseExpression(expectedToken, currentToken, lexer);
		Parameter newParameter = Parameter(newExpression);
		return newParameter;
	}
	return Parameter();
}

Expression * Queries::parseExpression(string expectedToken, Token currentToken, Lexer *lexer)
{

	// LEFT_PAREN
	currentToken = lexer->getCurrentToken(expectedToken);

	// Get parameter
	Parameter firstParameter = parseParameter(expectedToken, currentToken, lexer);

	string tokenType = lexer->peekAtCurrentToken().getTokenType();
	// Get operator (either ADD or MULTIPLY)
	if (tokenType == "ADD")
	{
		expectedToken = "ADD";
	}
	else
	{
		expectedToken = "MULTIPLY";
	}
	currentToken = lexer->getCurrentToken(expectedToken);
	string newOperator = currentToken.getString();

	Parameter secondParameter = parseParameter(expectedToken, currentToken, lexer);

	// RIGHT_PAREN
	expectedToken = "RIGHT_PAREN";
	currentToken = lexer->getCurrentToken(expectedToken);

	return new Expression(firstParameter, newOperator[0], secondParameter);
}

vector<Query> Queries::getQueries()
{
	return queries;
}

ostream & operator<<(std::ostream & os, Queries & Queries)
{
	// Print out header with number of queries
	os << "Queries(" << Queries.queries.size() << "):" << endl;
	// Print out the queries
	for (unsigned int i = 0; i< Queries.queries.size(); i++)
	{
		os << "  ";
		os << Queries.queries[i];
		os << endl;
	}
	return os;
}
