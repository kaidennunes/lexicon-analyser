#include "stdafx.h"
#include "RelationalDatabase.h"

RelationalDatabase::RelationalDatabase()
{
}
// TODO: Prune rules
RelationalDatabase::RelationalDatabase(DatalogParser DatalogParser)
{
	// Get queries
	queries = DatalogParser.getQueries();

	// Make tables with Schemes data
	parseSchemes(DatalogParser.getSchemes());

	// Add content to tables with Facts data
	parseFacts(DatalogParser.getFacts());

	// Get the rules
	vector<Rule> rules = DatalogParser.getRules();

	// Create the graph
	AdjacencyGraph dependencyGraph = AdjacencyGraph(rules);

	// Create a graph that shows the necessary execution order
	AdjacencyGraph mustBeExecutedBeforeGraph = dependencyGraph.invert();

	// Create a depth first search forest using the graph of execution order
	DFSForest dfsForest = DFSForest(mustBeExecutedBeforeGraph);

	// Create the topological sort of the forest
	vector<int> topologicallySortedForest = dfsForest.generateTopologicallySortedForest();

	// Invert the depth first search forest based on the main graph and topological sort of the forest
	DFSForest invertedDFSForest = DFSForest(dependencyGraph, topologicallySortedForest);

	// Create the data structure of the strongly connected components
	vector<vector<int>> stronglyConnectedComponents = invertedDFSForest.createStronglyConnectedComponents();

	// Set rules as trivial or non trivial
	dependencyGraph.setRuleTriviality(stronglyConnectedComponents, rules);

	// Print out dependency graph
	cout << "Dependency Graph";
	cout << endl;
	cout << dependencyGraphToString(dependencyGraph);

	// Sort the strongly connected components numerically
	sortStronglyConnectedComponents(stronglyConnectedComponents);

	// Add content to tables with Rules data, print times iterated through rules
	parseRules(stronglyConnectedComponents, rules);
}

RelationalDatabase::~RelationalDatabase()
{
}

void RelationalDatabase::executeQueries()
{
	cout << "Query Evaluation";
	cout << endl;
	// Execute queries
	for (unsigned int i = 0; i < queries.size(); i++)
	{
		Predicate query = queries[i].getPredicate();

		// Get name of table to perform query on
		string tableName = query.getId().getId();

		// Get list of parameters
		vector<Parameter> parameters = query.getParameters();

		// Get table
		Table selectedTable = tables[tableName];

		map<string, int> idList = map<string, int>();
		map<int, string> idListMap = map<int, string>();

		for (unsigned int j = 0; j < parameters.size(); j++)
		{
			// For all literals (actual strings), call select on it and update table
			if (parameters[j].isStringLiteral())
			{
				string columnData = parameters[j].getParameterData();
				ColumnValueParameters newColumnValueParameters = ColumnValueParameters(columnData, j);
				selectedTable = selectedTable.select(newColumnValueParameters);
			}
			// If it is an ID, put it in the map (if not already presenet) to select, project, and rename on ID's
			else
			{
				// If the ID is not in the map, add it and its column number
				if (idList.find(parameters[j].getParameterData()) == idList.end())
				{
					idList[parameters[j].getParameterData()] = j;
					idListMap[j] = parameters[j].getParameterData();
				}
				// If the ID is in the map, we need to make sure those two columns are equal
				else
				{
					int firstIdColumn = idList[parameters[j].getParameterData()];
					ColumnColumnParameters idColumns = ColumnColumnParameters(firstIdColumn, j);
					selectedTable = selectedTable.select(idColumns);
				}
			}
		}

		// Initialize vector and header for parameters in project and rename 
		vector<int> idVector = vector<int>();
		Headers resultHeader = Headers();

		// Put the unique ID's into vector and header so we can call project and rename on the table
		for (auto elem : idListMap)
		{
			idVector.push_back(elem.first);
			resultHeader.addHeader(elem.second);
		}

		// Call project and rename on the new table according to the ID's we found (if there are any ID's)
		if (idVector.size() != 0)
		{
			selectedTable = selectedTable.project(idVector);
			selectedTable = selectedTable.rename(resultHeader);
			cout << queries[i];
			cout << " ";

			// Print out the result of the query
			selectedTable.printTable(true);
		}
		else
		{
			cout << queries[i];
			cout << " ";
			selectedTable.printTable(false);
		}
	}
}

void RelationalDatabase::runTestCases(AdjacencyGraph mustBeExecutedBeforeGraph, vector<int> topologicallySortedForest, vector<vector<int>> stronglyConnectedComponents, string testNumber)
{
	cout << "Running Test Case ";
	cout << testNumber;
	cout << endl;
	string outputString = "";
	string outputStringFileLocation = "testcase" + testNumber + "Output.txt";
	outputString += dependencyGraphToString(mustBeExecutedBeforeGraph);
	outputString += "\n";
	outputString += stronglyConnectedComponentsToString(topologicallySortedForest);
	outputString += "\n";
	for (unsigned int i=0;i<stronglyConnectedComponents.size();i++)
	{
		outputString += stronglyConnectedComponentsToString(stronglyConnectedComponents[i]);
	}

	if (matchesExpectedOutput(outputString, outputStringFileLocation))
	{
		cout << "Test " << testNumber << " Succeeded!";
	}
	else
	{
		cout << "Test " << testNumber << " Failed!";
	}
	cout << endl;
	cout << endl;
}

void RelationalDatabase::parseSchemes(vector<Scheme> schemes)
{
	for (unsigned int i = 0; i < schemes.size(); i++)
	{
		Headers newHeaders = Headers();
		for (unsigned int j = 0; j < schemes[i].getHeaderNames().size(); j++)
		{
			newHeaders.addHeader(schemes[i].getHeaderNames()[j].getId());
		}
		Table newTable = Table(schemes[i].getName().getId(), newHeaders);
		tables[schemes[i].getName().getId()] = newTable;
	}
}

void RelationalDatabase::parseFacts(vector<Fact> facts)
{
	for (unsigned int i = 0; i < facts.size(); i++)
	{
		Row newRow = Row();
		for (unsigned int j = 0; j < facts[i].getColumnData().size(); j++)
		{
			newRow.addColumn(facts[i].getColumnData()[j].getString());
		}
		tables[facts[i].getTableName().getId()].addRow(newRow);
	}
}

void RelationalDatabase::parseRules(vector<vector<int>> stronglyConnectedComponents, vector<Rule> rules)
{
	cout << "Rule Evaluation";
	cout << endl;
	// Go through each strongly connected component
	for (unsigned int v = 0; v < stronglyConnectedComponents.size(); v++)
	{
		timesIteratedThroughRules = 0;
		string stronglyConnectedComponentsParts = stronglyConnectedComponentsToString(stronglyConnectedComponents[v]);
		bool rowsAddedToTable;
		do
		{
			rowsAddedToTable = false;
			// Go through each node in the strongly connected component
			for (unsigned int l = 0; l < stronglyConnectedComponents[v].size(); l++)
			{
				// Get list of predicates in rule
				vector<Predicate> predicateList = rules[stronglyConnectedComponents[v][l]].getPredicateList().getPredicates();

				// Initialize table to use as the joining table
				Table currentRuleTable = Table();

				// Go through all of the predicates in the rule, natural joining each one
				for (unsigned int j = 0; j < predicateList.size(); j++)
				{
					Predicate predicateRule = predicateList[j];

					// Get name of table for the predicate to query on
					string tableName = predicateRule.getId().getId();

					// Get list of parameters
					vector<Parameter> parameters = predicateRule.getParameters();

					// Get table
					Table selectedTable = tables[tableName];

					map<string, int> idList = map<string, int>();
					map<int, string> idListMap = map<int, string>();

					for (unsigned int k = 0; k < parameters.size(); k++)
					{
						// For all literals (actual strings), call select on it and update table
						if (parameters[k].isStringLiteral())
						{
							string columnData = parameters[k].getParameterData();
							ColumnValueParameters newColumnValueParameters = ColumnValueParameters(columnData, k);
							selectedTable = selectedTable.select(newColumnValueParameters);
						}
						// If it is an ID, put it in the map (if not already present) to select, project, and rename on ID's
						else
						{
							// If the ID is not in the map, add it and its column number
							if (idList.find(parameters[k].getParameterData()) == idList.end())
							{
								idList[parameters[k].getParameterData()] = k;
								idListMap[k] = parameters[k].getParameterData();
							}
							// If the ID is in the map, we need to make sure those two columns are equal
							else
							{
								int firstIdColumn = idList[parameters[k].getParameterData()];
								ColumnColumnParameters idColumns = ColumnColumnParameters(firstIdColumn, k);
								selectedTable = selectedTable.select(idColumns);
							}
						}
					}

					// Initialize vector and header for parameters in project and rename
					vector<int> idVector = vector<int>();
					Headers resultHeader = Headers();

					// Put the unique ID's into vector and header so we can call project and rename on the table
					for (auto elem : idListMap)
					{
						idVector.push_back(elem.first);
						resultHeader.addHeader(elem.second);
					}

					// Call project and rename on the new table according to the ID's we found (if there are any ID's)
					if (idVector.size() != 0)
					{
						selectedTable = selectedTable.project(idVector);
						selectedTable = selectedTable.rename(resultHeader);
					}
					// Natural join the tables if it isn't the first one to be created
					if (j != 0)
					{
						currentRuleTable = currentRuleTable.naturalJoin(selectedTable);
					}
					else
					{
						currentRuleTable = selectedTable;
					}
				}

				// Get the head predicate
				HeadPredicate headPredicateRule = rules[stronglyConnectedComponents[v][l]].getHeadPredicate();

				// Get name of table to perform union the result of the rule with
				string headPredicateTableName = headPredicateRule.getId().getId();

				// Get list of head predicate ids
				vector<Id> headPredicateIds = headPredicateRule.getIds();

				vector <int> columnsToProject = vector<int>();
				vector<string> ruleTableHeaders = currentRuleTable.getHeaders().getHeaders();

				// Find columns to project
				for (unsigned int k = 0; k < headPredicateIds.size(); k++)
				{
					for (unsigned int m = 0; m < ruleTableHeaders.size(); m++)
					{
						if (headPredicateIds[k].getId() == ruleTableHeaders[m])
						{
							columnsToProject.push_back(m);
							break;
						}
					}
				}

				// Now we project columns that appear in the head predicate
				currentRuleTable = currentRuleTable.project(columnsToProject);

				vector<string> databaseTableHeaders = tables[headPredicateTableName].getHeaders().getHeaders();

				// Create a new header for the rule
				Headers newRuleHeaders = Headers();
				for (unsigned int k = 0; k < databaseTableHeaders.size(); k++)
				{
					newRuleHeaders.addHeader(databaseTableHeaders[k]);
				}

				// Rename union table (rule) to match the headers of the table in the database
				currentRuleTable = currentRuleTable.rename(newRuleHeaders);

				// Find how many rows the database table has
				int numberOfRows = tables[headPredicateTableName].numberOfRows();

				// Union the tables
				tables[headPredicateTableName].tableUnion(currentRuleTable);

				// If there are more rows in the database table after the union, then at least one new row was added
				if (numberOfRows < tables[headPredicateTableName].numberOfRows())
				{
					rowsAddedToTable = true;
				}
				// If it is a trivial rule, we don't need to iterate on it, pretend no rows were added to break out of the iteration loop
				if (rules[stronglyConnectedComponents[v][l]].getIsTrivial())
				{
					rowsAddedToTable = false;
				}
			}
			timesIteratedThroughRules++;
		} while (rowsAddedToTable);

		// Print out number of times iterated through strongly connected component
		cout << timesIteratedThroughRules;
		cout << " passes: ";
		cout << stronglyConnectedComponentsParts;
		cout << endl;
	}
	cout << endl;
}

void RelationalDatabase::printTables()
{
	for (auto elem : tables) cout << elem.second << endl;
}

string RelationalDatabase::dependencyGraphToString(AdjacencyGraph dependencyGraph)
{
	stringstream ss;
	map<int, set<int>> graph = dependencyGraph.getGraph();
	for (auto rule : graph)
	{
		ss << "R";
		ss << rule.first;
		ss << ":";
		for (auto dependency : rule.second)
		{
			ss << "R";
			ss << dependency;
			if (dependency != *rule.second.rbegin()) ss << ",";
		}
		ss << endl;
	}
	ss << endl;
	return ss.str();
}

string RelationalDatabase::stronglyConnectedComponentsToString(vector<int> stronglyConnectedComponents)
{
	string connectedComponents = "";
	for (unsigned int i=0;i<stronglyConnectedComponents.size();i++)
	{
		connectedComponents += "R" + to_string(stronglyConnectedComponents[i]);
		if (stronglyConnectedComponents.size()-1 != i) connectedComponents += ",";
	}
	return connectedComponents;
}

bool RelationalDatabase::matchesExpectedOutput(string outputString, string expectedOutputFileLocation)
{
	// Get input from command line
	string fileLocation(expectedOutputFileLocation);

	// Try to open file
	ifstream file;
	file.open(fileLocation);

	// If the file is not valid, we will throw an error and quit
	if (!(file.is_open()))
	{
		return 0;
	}

	// If not, we will read the input into the Chars class
	string content((istreambuf_iterator<char>(file)),
		(istreambuf_iterator<char>()));

	content.erase(std::remove(content.begin(), content.end(), '\r'), content.end());

	bool test = outputString == content;
	return test;
}

void RelationalDatabase::sortStronglyConnectedComponents(vector<vector<int>> &stronglyConnectedComponents)
{
	for (unsigned int i = 0; i < stronglyConnectedComponents.size(); i++)
	{
		sort(stronglyConnectedComponents[i].begin(), stronglyConnectedComponents[i].end());
	}
}
