/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/
#include "stdafx.h"
#include "HeadPredicate.h"


HeadPredicate::HeadPredicate()
{
}

HeadPredicate::HeadPredicate(Id newInitialId, IdList newIdList)
{
	LEFT_PAREN = '(';
	initialId = newInitialId;
	idList = newIdList;
	RIGHT_PAREN = ')';
}

HeadPredicate::~HeadPredicate()
{
}

Id HeadPredicate::getId()
{
	return initialId;
}

vector<Id> HeadPredicate::getIds()
{
	return idList.getIds();
}

ostream & operator<<(std::ostream & os, HeadPredicate & HeadPredicate) {
	os << HeadPredicate.initialId;
	os << HeadPredicate.LEFT_PAREN;
	os << HeadPredicate.idList;
	os << HeadPredicate.RIGHT_PAREN;
	return os;
}