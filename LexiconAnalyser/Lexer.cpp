/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Define lexer class. Prints tokens. Parses tokens
*/
#include "stdafx.h"
#include "Lexer.h"

// Prints out the tokens in the format: (Token_Type,"Value",Line_Number)
void Lexer::printTokens()
{
	for (unsigned int i = 0; i<tokens.size(); i++)
	{
		cout << tokens[i];
	}
	cout << "Total Tokens = " << tokens.size();
}

// Parse the input into tokens
void Lexer::parseIntoTokens(Chars input)
{
	unsigned int lineNumber = 1;
	// While we aren't at the end of the input, parse the input
	while (!input.eof())
	{
		// Determine what token type it is
		switch (input.currentChar()) {
		case '\n':
			lineNumber++;
			break;
		case ' ':
			break;
		case '\t':
			break;
		case '\v':
			break;
		case ',':
			createAddToken("COMMA", ",", lineNumber);
			break;
		case '.':
			createAddToken("PERIOD", ".", lineNumber);
			break;
		case '?':
			createAddToken("Q_MARK", "?", lineNumber);
			break;
		case '(':
			createAddToken("LEFT_PAREN", "(", lineNumber);
			break;
		case ')':
			createAddToken("RIGHT_PAREN", ")", lineNumber);
			break;
		case '*':
			createAddToken("MULTIPLY", "*", lineNumber);
			break;
		case '+':
			createAddToken("ADD", "+", lineNumber);
			break;
		case ':':
			// See if it is a colon or a colon dash
			colonOrColonDash(input, lineNumber);
			break;
		case '\'':
			// See if it is a string
			string(input, lineNumber);
			break;
		case '#':
			// See if it is a comment
			comment(input, lineNumber);
			break;
		default:
			// See if it is an id
			id(input, lineNumber);
			break;
		}
		// Increment the current token
		input.increment();
	}
}

void Lexer::colonOrColonDash(Chars &input, unsigned int startLine)
{
	std::string inputType = "COLON";
	std::string inputCharacters = ":";
	// Increment the input index
	input.increment();
	// If next character is a dash, it is a colon dash
	if (!input.eof() && input.currentChar() == '-')
	{
		inputType = "COLON_DASH";
		inputCharacters += "-";
	}
	// If not, it is just a colon
	else
	{
		input.decrement();
	}
	createAddToken(inputType, inputCharacters, startLine);
}

// See if it is a command or an id
void Lexer::id(Chars &input, unsigned int &lineNumber)
{
	unsigned int startLine = lineNumber;
	std::string inputType = "ID";
	std::string inputCharacters = "";

	// Add current character only if not empty (at end of file)
	if (input.currentChar() != '\0') inputCharacters += input.currentChar();

	// If it starts with a number or with anything but a letter, it is not an id
	if (isdigit(input.currentChar()) || !isalpha(input.currentChar()))
	{
		// If it is empty, this is the eof
		if (input.currentChar() == '\0') inputType = "EOF";
		else inputType = "UNDEFINED";
		createAddToken(inputType, inputCharacters, startLine);
		return;
	}
	// Otherwise, we will continue parsing
	while (true)
	{
		// Increment the input index
		input.increment();
		// If at end of file, we must end parsing
		if (input.eof())
		{
			// If it is just the eof, make a token
			if (inputCharacters.empty()) inputType = "EOF";
			break;
		}
		// If at end of line, we must end the parsing and increment the line number
		else if (input.currentChar() == '\n')
		{
			lineNumber++;
			break;
		}
		// Otherwise, add the character to the id if it is valid
		else if (isalpha(input.currentChar()) || isdigit(input.currentChar()))
		{
			inputCharacters += input.currentChar();
		}
		// If not, end the parsing
		else
		{
			input.decrement();
			break;
		}
	}
	// See if it is a keyword 
	if (inputCharacters == "Schemes")
	{
		inputType = "SCHEMES";
	}
	else if (inputCharacters == "Facts")
	{
		inputType = "FACTS";
	}
	else if (inputCharacters == "Rules")
	{
		inputType = "RULES";
	}
	else if (inputCharacters == "Queries")
	{
		inputType = "QUERIES";
	}
	createAddToken(inputType, inputCharacters, startLine);
}

// Parse out the string
void Lexer::string(Chars &input, unsigned int &lineNumber)
{
	unsigned int startLine = lineNumber;
	std::string inputType = "STRING";
	std::string inputCharacters = "'";
	while (true)
	{
		// Increment the input index
		input.increment();
		// If we are at the end of the file, we must create an undefined token
		if (input.eof())
		{
			inputType = "UNDEFINED";
			break;
		}
		// Otherwise we continue to parse the string
		else
		{
			// If it is another quote, we must check to see if it is the end of the string or if the string has a double quote
			if (input.currentChar() == '\'')
			{
				// Increment it again
				input.increment();
				if (input.eof())
				{
					inputCharacters += '\'';
					break;
				}
				// If it is a double string, then include it in the content.
				else if (input.currentChar() == '\'')
				{
					inputCharacters += "\'\'";
				}
				// Otherwise, it is the end of the string
				else
				{
					input.decrement();
					inputCharacters += '\'';
					break;
				}
			}
			// If it is a line break, we must add the extra line number and add the line break
			else if (input.currentChar() == '\n')
			{
				lineNumber++;
				inputCharacters += input.currentChar();
			}
			// If it is the end of file empty character, it is undefined
			else if (input.currentChar() == '\0')
			{
				inputType = "UNDEFINED";
				input.decrement();
				break;
			}
			else
			{
				// Add the next character to the token content
				inputCharacters += input.currentChar();
			}
		}
	}
	createAddToken(inputType, inputCharacters, startLine);
}

// Parse out the comment
void Lexer::comment(Chars &input, unsigned int &lineNumber)
{
	unsigned int startLine = lineNumber;
	std::string inputType = "COMMENT";
	std::string inputCharacters = "#";
	bool isMultiComment = false;
	bool hasMultiEnding = false;
	// Increment the input index
	input.increment();
	// If we are at the end of the file, we must end the parsing (it is an empty comment, but still valid)
	if (input.eof())
	{
//		createAddToken(inputType, inputCharacters, startLine);
		// Add eof token
		createAddToken("EOF", "", lineNumber);
		return;
	}
	// If the next character is a | then we know it is a multiblock comment
	else if (input.currentChar() == '|')
	{
		inputCharacters += "|";
		isMultiComment = true;
	}
	// If it is a single comment (empty) with new line, we must create the token and move on
	else if (!isMultiComment && input.currentChar() == '\n')
	{
//		createAddToken(inputType, inputCharacters, startLine);
		lineNumber++;
		return;
	}
	// Otherwise we just add the character and move on with our parsing loop
	else if (input.currentChar() != '\0')
	{
		inputCharacters += input.currentChar();
	}
	while (true)
	{
		// Increment the input index
		input.increment();
		// If we are at the end of the file and it is a multiline comment, we must create an undefined token
		if (isMultiComment && input.eof())
		{
			inputType = "UNDEFINED";
			createAddToken(inputType, inputCharacters, startLine);
			break;
		}
		// If it is a multiline comment and the end of the line, increment the line number and add the line break
		else if (isMultiComment && input.currentChar() == '\n')
		{
			hasMultiEnding = false;
			inputCharacters += input.currentChar();
			lineNumber++;
		}
		// If it is a multiline comment and ending
		else if (isMultiComment && input.currentChar() == '|')
		{
			inputCharacters += input.currentChar();
			hasMultiEnding = true;
		}
		// If it is the actual end of the multiline comment, break out
		else if (hasMultiEnding && input.currentChar() == '#')
		{
			inputCharacters += input.currentChar();
			break;
		}
		// If we are at the end of the file or the end of the line, but it is a normal comment, then it is just the end of the parsing
		else if (!isMultiComment && input.currentChar() == '\n')
		{
			lineNumber++;
			break;
		}
		else if (!isMultiComment && input.eof())
		{
			// Add comment token			
//			createAddToken(inputType, inputCharacters, startLine);
			// Add eof token
			createAddToken("EOF", "", lineNumber);
			return;
		}
		// If it is the end of file empty character, it is undefined
		else if (isMultiComment && input.currentChar() == '\0')
		{
			inputType = "UNDEFINED";
			input.decrement();
			createAddToken(inputType, inputCharacters, startLine);
			break;
		}
		// Otherwise we continue to parse the comment
		else if (input.currentChar() != '\0')
		{
			hasMultiEnding = false;
			// Add the next character to the token content
			inputCharacters += input.currentChar();
		}
	}
//	createAddToken(inputType, inputCharacters, startLine);
}

void Lexer::createAddToken(std::string type, std::string content, unsigned int lineNumber)
{
	// Create token
	Token newToken(content, type, lineNumber);
	// Add token to vector
	tokens.push_back(newToken);
}

Lexer::Lexer()
{
}

Lexer::~Lexer()
{
}

int Lexer::getNumberOfTokens()
{
	return tokens.size();
}

Token Lexer::getCurrentToken(std::string tokenType)
{
	Token nextToken = tokens.front();
	tokens.erase(tokens.begin());
	if (nextToken.getTokenType() != tokenType)
	{
		throw nextToken;
	}
	return nextToken;
}

Token Lexer::peekAtCurrentToken()
{
	return tokens.front();
}

