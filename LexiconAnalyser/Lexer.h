/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/
#pragma once

#include "Token.h"
#include "Chars.h"

using namespace std;

class Lexer {
private:
	vector<Token> tokens;
	unsigned int currentTokenLocation;
	void colonOrColonDash(Chars &input, unsigned int startLine);
	void id(Chars &input, unsigned int &lineNumber);
	void string(Chars &input, unsigned int &lineNumber);
	void comment(Chars &input, unsigned int &lineNumber);
	void createAddToken(std::string type, std::string content, unsigned int lineNumber);
public:
	Lexer();
	~Lexer();
	int getNumberOfTokens();
	Token getCurrentToken(std::string tokenType);
	Token peekAtCurrentToken();
	void printTokens();
	void parseIntoTokens(Chars input);
};
