#include "stdafx.h"
#include "ColumnValueParameters.h"


ColumnValueParameters::ColumnValueParameters()
{
}

ColumnValueParameters::ColumnValueParameters(string newFirstColumn, int newSecondColumn)
{
	firstColumn = newFirstColumn;
	secondColumn = newSecondColumn;
}


ColumnValueParameters::~ColumnValueParameters()
{
}

string ColumnValueParameters::getFirstColumn()
{
	return firstColumn;
}

int ColumnValueParameters::getSecondColumn()
{
	return secondColumn;
}
