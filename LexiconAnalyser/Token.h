/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create token class
*/
#pragma once

using namespace std;

class Token {

private:
	string characters;
	string tokenType;
	unsigned int lineNumber;
public:
	Token(string inputCharacters, string inputTokenType, unsigned int inputLineNumber);
	~Token();
	string getString();
	string getTokenType();
	friend ostream& operator <<(ostream &os, Token &token);
};