#pragma once

using namespace std;

#include "String.h"

class StringList
{
private:
	vector<String> strings;
public:
	StringList();
	~StringList();
	void addString(String string);
	vector<String> getStrings();
	friend ostream& operator <<(ostream &os, StringList &StringList);
};

