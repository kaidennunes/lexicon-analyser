/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/

#pragma once

#include "Scheme.h"
#include "Lexer.h"
#include "Id.h"
#include "IdList.h"

using namespace std;

class Schemes
{
private:
	vector<Scheme> schemes;
public:
	Schemes();
	~Schemes();
	void parseScheme(string expectedToken, Lexer *lexer);
	vector<Scheme> getSchemes();
	friend ostream& operator <<(ostream &os, Schemes &Schemes);
};

