#include "stdafx.h"
#include "AdjacencyGraph.h"

AdjacencyGraph::AdjacencyGraph()
{
}

AdjacencyGraph::AdjacencyGraph(vector<Rule> &rules)
{
	// Go through every rule
	for (unsigned int i=0;i<rules.size();i++)
	{
		// Get the predicates in the rule
		vector<Predicate> predicates = rules[i].getPredicateList().getPredicates();
		set<int> mappingNodes = set<int>();
		// Go through every predicate
		for (unsigned int j=0;j<predicates.size();j++)
		{
			// Check if predicate matches the head predicate of some other rule
			for (unsigned int k = 0; k < rules.size(); k++)
			{
				// If the head predicate of some rule and the predicate of the current rule match, add that rule index to the dependency graph
				if (rules[k].getHeadPredicate().getId().getId() == predicates[j].getId().getId())
				{
					// If the rule is recursive, it is not trivial
					if (rules[i].getHeadPredicate().getId().getId() == rules[j].getHeadPredicate().getId().getId()) rules[i].setTrivial(false);
					mappingNodes.insert(k);
				}
			}
		}
		graph[i] = mappingNodes;
	}
}

AdjacencyGraph::AdjacencyGraph(map<int, set<int>> newGraph)
{
	graph = newGraph;
}

AdjacencyGraph AdjacencyGraph::invert()
{
	// Initialize new graph
	map<int, set<int>> newGraph = map<int, set<int>>();

	// Got through each rule in the dependency graph
	for (auto rule : graph)
	{
		if (newGraph[rule.first].size() == 0)
		{
			newGraph[rule.first] == set<int>();
		}
		// Assign the current rule index to the rule indexes it was dependent on
		for (auto ruleIndex : rule.second)
		{
			newGraph[ruleIndex].insert(rule.first);
		}
	}
	return AdjacencyGraph(newGraph);
}

map<int, set<int>> AdjacencyGraph::getGraph()
{
	return graph;
}


void AdjacencyGraph::setRuleTriviality(vector<vector<int>> stronglyConnectedComponents, vector<Rule> &rules)
{
	// Go through each set of strongly connected components
	for (unsigned int i = 0; i<stronglyConnectedComponents.size(); i++)
	{
		// If a part of that component is dependent on any other part, we must mark it as non trivial
		for (unsigned int j = 0; j<stronglyConnectedComponents[i].size(); j++)
		{
			for (auto dependency : graph[stronglyConnectedComponents[i][j]])
			{
				if (dependency == NULL) continue;
				for (unsigned int k = 0; k<stronglyConnectedComponents[i].size(); k++)
				{
					if (dependency == stronglyConnectedComponents[i][k])
					{
						rules[dependency].setTrivial(false);
					}
				}
			}
		}
	}
}

AdjacencyGraph::~AdjacencyGraph()
{
}
