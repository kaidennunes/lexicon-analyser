/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Defines character class.
*/
#include "stdafx.h"
#include "Chars.h"

Chars::Chars(string inputCharacters)
{
	characters = inputCharacters;
	currentIndex = 0;
}


Chars::~Chars()
{
}

bool Chars::eof() const
{
	return eof(currentIndex);
}

bool Chars::eof(unsigned int index) const
{
	if (characters.length() >= index)
	{
		return false;
	}
	else
	{
		return true;
	}
}

char Chars::currentChar()
{
	return characters[currentIndex];
}

void Chars::increment()
{
	currentIndex++;
}

void Chars::decrement()
{
	currentIndex--;
}