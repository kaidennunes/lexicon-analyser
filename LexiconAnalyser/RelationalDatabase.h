#pragma once

#include <map>
#include <fstream>
#include <algorithm>
#include "DatalogParser.h"
#include "Table.h"
#include "Headers.h"
#include "AdjacencyGraph.h"
#include "DFSForest.h"

class RelationalDatabase
{
private: 
	map<string,Table> tables;
	vector<Query> queries;
	int timesIteratedThroughRules;
	void parseSchemes(vector<Scheme> schemes);
	void parseFacts(vector<Fact> facts);
	void parseRules(vector<vector<int>> stronglyConnectedComponents, vector<Rule> rules);
	void printTables();
	string dependencyGraphToString(AdjacencyGraph dependencyGraph);
	string stronglyConnectedComponentsToString(vector<int> stronglyConnectedComponents);
	bool matchesExpectedOutput(string outputString, string expectedOutputFileLocation);
	void sortStronglyConnectedComponents(vector<vector<int>> &stronglyConnectedComponents);
public:
	RelationalDatabase();
	RelationalDatabase(DatalogParser DatalogParser);
	~RelationalDatabase();
	void executeQueries();
	void runTestCases(AdjacencyGraph mustBeExecutedBeforeGraph, vector<int> topologicallySortedForest, vector<vector<int>> stronglyConnectedComponents, string testNumber);
};