#pragma once

#include "Id.h"
#include "ParameterList.h"


class Predicate
{
private:
	Id id;
	char LEFT_PAREN;
	ParameterList parameterList;
	char RIGHT_PAREN;
public:
	Predicate();
	~Predicate();
	Predicate(Id newId, ParameterList newParameterList);
	Id getId();
	vector<Parameter> getParameters();
	friend ostream& operator <<(ostream &os, Predicate &Predicate);
};

