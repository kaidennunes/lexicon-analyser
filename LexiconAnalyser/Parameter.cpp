#include "stdafx.h"
#include "Parameter.h"
#include "Expression.h"


Parameter::Parameter()
{
}


Parameter::~Parameter()
{
}

Parameter::Parameter(Id newParameter)
{
	idParameter = newParameter;
	expression = NULL;
}

Parameter::Parameter(String newParameter)
{
	stringParameter = newParameter;
	expression = NULL;
}


Parameter::Parameter(Expression* newExpression)
{
	expression = newExpression;
}

string Parameter::getParameterData()
{
	
	if (idParameter.getId() != "") return idParameter.getId();
	else if (stringParameter.getString() != "") return stringParameter.getString();
	else return "";
}

bool Parameter::isStringLiteral()
{
	if (stringParameter.getString() != "") return true;
	return false;
}

ostream & operator<<(std::ostream & os, Parameter & Parameter) {
	os << Parameter.idParameter;
	os << Parameter.stringParameter;
	if (Parameter.expression != NULL) os << *(Parameter.expression);
	return os;
}
