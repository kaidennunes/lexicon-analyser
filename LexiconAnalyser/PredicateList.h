#pragma once

#include "Predicate.h"

using namespace std;

class PredicateList
{
private:
	vector<Predicate> predicates;
public:
	PredicateList();
	~PredicateList();
	void addPredicate(Predicate predicate);
	vector<Predicate> getPredicates();
	friend ostream& operator <<(ostream &os, PredicateList &PredicateList);
};

