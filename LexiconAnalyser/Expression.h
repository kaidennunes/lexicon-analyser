#ifndef EXPRESSION_H
#define EXPRESSION_H

#include "Parameter.h"

using namespace std;

class Expression : public Parameter
{
private:
	char LEFT_PAREN;
	Parameter firstParameter;
	char middleOperator;
	Parameter secondParameter;
	char RIGHT_PAREN;
public:
	Expression();
	Expression(Parameter newFirstParameter, char newMiddleOperator, Parameter newSecondParameter);
	~Expression();
	friend ostream& operator <<(ostream &os, Expression &Expression);
};
#endif

