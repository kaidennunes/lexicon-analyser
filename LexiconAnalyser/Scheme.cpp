/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/
#include "stdafx.h"
#include "Scheme.h"

Scheme::Scheme()
{

}

Scheme::Scheme(Id newInitialId, IdList newIdList)
{
	LEFT_PAREN = '(';
	initialId = newInitialId;
	idList = newIdList;
	RIGHT_PAREN = ')';
}

Scheme::~Scheme()
{
}

Id Scheme::getName()
{
	return initialId;
}

vector<Id> Scheme::getHeaderNames()
{
	vector<Id> headerNames = idList.getIds();
	return headerNames;
}

ostream & operator<<(std::ostream & os, Scheme & Scheme) {
	os << Scheme.initialId;
	os << Scheme.LEFT_PAREN;
	os << Scheme.idList;
	os << Scheme.RIGHT_PAREN;
	return os;
}