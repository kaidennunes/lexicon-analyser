#include "stdafx.h"
#include "String.h"


String::String()
{
}

String::String(Lexer * lexer)
{
	characters = lexer->getCurrentToken("STRING").getString();
}

String::~String()
{
}

string String::getString()
{
	return characters;
}

ostream & operator<<(std::ostream & os, String & String) {
	os << String.characters;
	return os;
}
