#include "stdafx.h"
#include "StringList.h"


StringList::StringList()
{
}


StringList::~StringList()
{
}

void StringList::addString(String string)
{
	strings.push_back(string);
}

vector<String> StringList::getStrings()
{
	return strings;
}

ostream & operator<<(std::ostream & os, StringList & StringList) {
	// Print out the schemes
	for (unsigned int i = 0; i < StringList.strings.size(); i++)
	{
		os << ',';
		os << StringList.strings[i];
	}
	return os;
}