#include "stdafx.h"
#include "DFSForest.h"

DFSForest::DFSForest()
{
}

DFSForest::~DFSForest()
{
}

DFSForest::DFSForest(AdjacencyGraph mustBeExecutedBeforeGraph)
{
	// Get map from graph
	map<int, set<int>> graph = mustBeExecutedBeforeGraph.getGraph();
	// Settled nodes
	map<int, bool> settledNodes = map<int, bool>();
	for (auto rule : graph)
	{
		if (settledNodes[rule.first] != true)
		{
			settledNodes[rule.first] = true;
			vector<int> nodes = vector<int>();
			nodes.push_back(rule.first);
			forest.push_back(iterateThroughMap(nodes, settledNodes, rule, graph));
		}
	}
}

DFSForest::DFSForest(AdjacencyGraph dependencyGraph, vector<int> topologicallySortedForest)
{
	// Get map from graph
	map<int, set<int>> graph = dependencyGraph.getGraph();
	// Settled nodes
	map<int, bool> settledNodes = map<int, bool>();
	for (unsigned int i=0;i<topologicallySortedForest.size();i++)
	{
		if (settledNodes[topologicallySortedForest[i]] != true)
		{
			settledNodes[topologicallySortedForest[i]] = true;
			vector<int> nodes = vector<int>();
			nodes.push_back(topologicallySortedForest[i]);
			pair<int, set<int>> ruleToBeEvaluated(topologicallySortedForest[i], graph[topologicallySortedForest[i]]);
			forest.push_back(iterateThroughMap(nodes, settledNodes, ruleToBeEvaluated, graph));
		}
	}
}

vector<int> DFSForest::iterateThroughMap(vector<int> &ruleDependencies, map<int, bool> &settledNodes, pair<int, set<int>> rule, map<int, set<int>> graph)
{
	// Set that we have settled this node
	settledNodes[rule.first] = true;
	for (auto dependency : rule.second)
	{
		// If we have settled this node, ignore it
		if (settledNodes[dependency] == true) continue;
		ruleDependencies.push_back(dependency);
		pair<int, set<int>> ruleToBeEvaluated (dependency, graph[dependency]);
		iterateThroughMap(ruleDependencies, settledNodes, ruleToBeEvaluated, graph);
	}
	return ruleDependencies;
}

vector<int> DFSForest::generateTopologicallySortedForest()
{
	stack<int> forestStack = stack<int>();
	vector<int> topologicallySortedForest = vector<int>();
	for (unsigned int i=0;i<forest.size();i++)
	{
		for (unsigned int j = forest[i].size(); j-- != 0; )
		{
			forestStack.push(forest[i][j]);
		}
	}
	while (!forestStack.empty())
	{
		topologicallySortedForest.push_back((forestStack.top()));
		forestStack.pop();
	}
	return topologicallySortedForest;
}

vector<vector<int>> DFSForest::createStronglyConnectedComponents()
{
	return forest;
}
