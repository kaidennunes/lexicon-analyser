/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/

#pragma once

#include "Id.h"
#include "IdList.h"

using namespace std;

class Scheme
{
private:
	Id initialId;
	char LEFT_PAREN;
	IdList idList;
	char RIGHT_PAREN;
public:
	Scheme();
	Scheme(Id newInitialId, IdList newIdList);
	~Scheme();
	Id getName();
	vector<Id> getHeaderNames();
	friend ostream& operator <<(ostream &os, Scheme &Scheme);
};

