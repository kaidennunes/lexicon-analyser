/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create character class. Allows parsing of a file string.
*/
#pragma once

#include "Rule.h"
#include "Query.h"
#include <map>
#include <set>

class AdjacencyGraph
{
private:
	map<int, set<int>> graph = map<int, set<int>>();
public:
	AdjacencyGraph();
	AdjacencyGraph(vector<Rule> &rules);
	AdjacencyGraph(map<int, set<int>> newGraph);
	AdjacencyGraph invert();
	map<int, set<int>> getGraph();
	void setRuleTriviality(vector<vector<int>> stronglyConnectedComponents, vector<Rule> &rules);
	~AdjacencyGraph();
};