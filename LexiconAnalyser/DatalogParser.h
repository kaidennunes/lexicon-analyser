/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Datalog Parser class, which determines if the tokens generated from the file input (with the lexer) are valid
*/
#pragma once

#include "Lexer.h"
#include "Schemes.h"
#include "Facts.h"
#include "Rules.h"
#include "Queries.h"

using namespace std;

class DatalogParser {
private:
	Schemes schemes;
	Facts facts;
	Rules rules;
	Queries queries;
public:
	DatalogParser();
	~DatalogParser();
	void parseTokens(Lexer lexer);
	vector<Scheme> getSchemes();
	vector<Fact> getFacts();
	vector<Rule> getRules();
	vector<Query> getQueries();
	void printData();
};
