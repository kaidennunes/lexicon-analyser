#include "stdafx.h"
#include "ColumnColumnParameters.h"


ColumnColumnParameters::ColumnColumnParameters()
{
}

ColumnColumnParameters::ColumnColumnParameters(int newFirstColumn, int newSecondColumn)
{
	firstColumn = newFirstColumn;
	secondColumn = newSecondColumn;
}


ColumnColumnParameters::~ColumnColumnParameters()
{
}

int ColumnColumnParameters::getFirstColumn()
{
	return firstColumn;
}

int ColumnColumnParameters::getSecondColumn()
{
	return secondColumn;
}
