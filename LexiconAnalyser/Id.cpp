#include "stdafx.h"
#include "Id.h"

Id::Id()
{

}

Id::Id(Lexer * lexer)
{
	id = lexer->getCurrentToken("ID").getString();
}

Id::~Id()
{
}

string Id::getId()
{
	return id;
}

ostream & operator<<(std::ostream & os, Id & Id) {
	os << Id.id;
	return os;
}
