#ifndef PARAMETER_H
#define PARAMETER_H


class Expression;

using namespace std;

#include "String.h"
#include "Id.h"

class Parameter
{
protected:
	Id idParameter;
	String stringParameter;
	Expression* expression;
public:
	Parameter();
	~Parameter();
	Parameter(Id newParameter);
	Parameter(String newParameter);
	Parameter(Expression* newExpression);
	string getParameterData();
	bool isStringLiteral();
	friend ostream& operator <<(ostream &os, Parameter &Parameter);
};
#endif
