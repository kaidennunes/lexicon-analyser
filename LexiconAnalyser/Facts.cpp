/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/

#include "stdafx.h"
#include "Facts.h"


Facts::Facts()
{
}


Facts::~Facts()
{
}

void Facts::parseScheme(string expectedToken, Lexer *lexer)
{
	// Get current token (should be a FACTS token)
	Token currentToken = lexer->getCurrentToken(expectedToken);

	// We now expect a colon
	expectedToken = "COLON";
	currentToken = lexer->getCurrentToken(expectedToken);

	// Parse through all the tokens until we finishing the facts section
	while (lexer->getNumberOfTokens() > 0 && lexer->peekAtCurrentToken().getTokenType() != "RULES")
	{
		// We now expect facts (ID first)
		Id id = Id(lexer);

		expectedToken = "LEFT_PAREN";
		currentToken = lexer->getCurrentToken(expectedToken);

		String string = String(lexer);

		// Add to domain
		domain.push_back(string.getString());

		StringList stringList = StringList();
		stringList.addString(string);

		while (lexer->peekAtCurrentToken().getTokenType() == "COMMA")
		{
			expectedToken = "COMMA";
			currentToken = lexer->getCurrentToken(expectedToken);

			String tempString = String(lexer);

			// Add to domain
			domain.push_back(tempString.getString());

			stringList.addString(tempString);
		}

		expectedToken = "RIGHT_PAREN";
		currentToken = lexer->getCurrentToken(expectedToken);

		expectedToken = "PERIOD";
		currentToken = lexer->getCurrentToken(expectedToken);

		Fact newFact = Fact(id, stringList);
		facts.push_back(newFact);
	}
}

void Facts::printDomain()
{
	// Sort domain
	sort(domain.begin(), domain.end());
	// Eliminate duplicates
	domain.erase(unique(domain.begin(), domain.end()), domain.end());
	// Print domain
	cout << "Domain(" << domain.size() << "):" << endl;
	for (unsigned int i = 0; i < domain.size(); i++)
	{
		cout << "  ";
		cout << domain[i];
		// Add new lines unless it is the last domain
		if (i != domain.size() - 1) cout << endl;
	}
}

vector<Fact> Facts::getFacts()
{
	return facts;
}

ostream & operator<<(std::ostream & os, Facts & Facts) {
	// Print out header with number of facts
	os << "Facts(" << Facts.facts.size() << "):" << endl;
	// Print out the facts
	for (unsigned int i = 0; i< Facts.facts.size();i++)
	{
		os << "  ";
		os << Facts.facts[i];
		os << endl;
	}
	return os;
}
