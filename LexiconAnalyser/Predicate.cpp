#include "stdafx.h"
#include "Predicate.h"


Predicate::Predicate()
{
}

Predicate::Predicate(Id newId, ParameterList newParameterList)
{
	LEFT_PAREN = '(';
	id = newId;
	parameterList = newParameterList;
	RIGHT_PAREN = ')';
}

Id Predicate::getId()
{
	return id;
}

vector<Parameter> Predicate::getParameters()
{
	return parameterList.getParameters();
}

Predicate::~Predicate()
{
}

ostream & operator<<(std::ostream & os, Predicate & Predicate) {
	os << Predicate.id;
	os << Predicate.LEFT_PAREN;
	os << Predicate.parameterList;
	os << Predicate.RIGHT_PAREN;
	return os;
}