/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/
#pragma once

#include "Id.h"
#include "IdList.h"

using namespace std;

class HeadPredicate
{
private:
	Id initialId;
	char LEFT_PAREN;
	IdList idList;
	char RIGHT_PAREN;
public:
	HeadPredicate();
	HeadPredicate(Id newInitialId, IdList newIdList);
	~HeadPredicate();
	Id getId();
	vector<Id> getIds();
	friend ostream& operator <<(ostream &os, HeadPredicate &HeadPredicate);
};

