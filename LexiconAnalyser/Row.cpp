#include "stdafx.h"
#include "Row.h"


Row::Row()
{
}

Row::~Row()
{
}

Row Row::modifyRows(vector<int> columnIndexes)
{
	Row newRow = Row();
	for (unsigned int i = 0; i < columnIndexes.size(); i++)
	{
		newRow.addColumn(data[columnIndexes[i]]);
	}
	return Row(newRow);
}

void Row::addColumn(string newData)
{
	data.push_back(newData);
}

bool Row::hasValueAtIndex(int index, string value)
{
	string columnData = data[index];
	return  columnData == value;
}

bool Row::indexesHaveSameValue(int firstIndex, int secondIndex)
{
	string firstColumnData = data[firstIndex];
	string secondColumnData = data[secondIndex];
	return  firstColumnData == secondColumnData;
}

void Row::printRow(vector<string> headers)
{
	for (unsigned int i = 0; i<data.size(); i++)
	{
		cout << headers[i];
		cout << "=";
		cout << data[i];
		if (data.size() - 1 != i) cout << ", ";
	}
}

bool Row::matchs(Row matchRow, set<ColumnColumnParameters> columnMappings)
{
	// If the row is the same as the other row in the common columns, it matches
	for (auto columnMapping : columnMappings)
	{
		if (data[columnMapping.getFirstColumn()] != matchRow.data[columnMapping.getSecondColumn()])
		{
			return false;
		}
	}
	return true;
}

Row Row::mergeWith(Row mergeRow, vector<int> columnsToMerge)
{
	// Initialize the new row
	Row newRow = Row();

	// Union the rows
	for (unsigned int i = 0; i<data.size(); i++)
	{
		newRow.addColumn(data[i]);
	}
	for (unsigned int i = 0; i<columnsToMerge.size(); i++)
	{
		newRow.addColumn(mergeRow.data[columnsToMerge[i]]);
	}
	return newRow;
}

Row Row::rearrange(vector<int> mapping)
{
	// Initialize the new row
	Row newRow = Row();
	
	// Add the columns to the row in the proper order 
	for (unsigned int i = 0; i<mapping.size(); i++)
	{
		newRow.addColumn(data[mapping[i]]);
	}
	return newRow;
}

ostream & operator<<(std::ostream & os, Row & Row)
{
	for (unsigned int i = 0; i<Row.data.size(); i++)
	{
		os << Row.data[i];
		if (Row.data.size() - 1 != i) os << ", ";
	}
	return os;
}
