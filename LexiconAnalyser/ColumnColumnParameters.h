#pragma once
#include "ColumnParameters.h"
class ColumnColumnParameters :
	public ColumnParameters
{
private:
	int firstColumn;
	int secondColumn;
public:
	ColumnColumnParameters();
	ColumnColumnParameters(int newFirstColumn, int newSecondColumn);
	~ColumnColumnParameters();
	int getFirstColumn();
	int getSecondColumn();
	bool operator == (const ColumnColumnParameters& columnColumnParameters) const {
		return (firstColumn == columnColumnParameters.firstColumn && secondColumn == columnColumnParameters.secondColumn);
	}
	bool operator < (const ColumnColumnParameters& columnColumnParameters) const {
		return firstColumn < columnColumnParameters.firstColumn;
	}
	bool operator > (const ColumnColumnParameters& columnColumnParameters) const {
		return firstColumn > columnColumnParameters.firstColumn;
	}
};

