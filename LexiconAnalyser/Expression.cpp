#include "stdafx.h"
#include "Expression.h"


Expression::Expression()
{
}

Expression::Expression(Parameter newFirstParameter, char newMiddleOperator, Parameter newSecondParameter) : Parameter()
{
	LEFT_PAREN = '(';
	firstParameter = newFirstParameter;
	middleOperator = newMiddleOperator;
	secondParameter = newSecondParameter;
	RIGHT_PAREN = ')';
}


Expression::~Expression()
{
}

ostream & operator<<(std::ostream & os, Expression & Expression) {
	os << Expression.LEFT_PAREN;
	os << Expression.firstParameter;
	os << Expression.middleOperator;
	os << Expression.secondParameter;
	os << Expression.RIGHT_PAREN;
	return os;
}
