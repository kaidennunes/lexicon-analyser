/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/

#pragma once

#include "Id.h"
#include "String.h"
#include "StringList.h"

using namespace std;

class Fact
{
private:
	Id id;
	char LEFT_PAREN;
	StringList stringList;
	char RIGHT_PAREN;
	char PERIOD;
public:
	Fact();
	Fact(Id newId, StringList newStringList);
	~Fact();
	Id getTableName();
	vector<String> getColumnData();
	friend ostream& operator <<(ostream &os, Fact &Fact);
};

