/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/

#include "stdafx.h"
#include "Fact.h"

Fact::Fact()
{
}

Fact::Fact(Id newId, StringList newStringList)
{
	LEFT_PAREN = '(';
	id = newId;
	stringList = newStringList;
	RIGHT_PAREN = ')';
	PERIOD = '.';
}

Fact::~Fact()
{
}

Id Fact::getTableName()
{
	return id;
}

vector<String> Fact::getColumnData()
{
	vector<String> columnData = stringList.getStrings();
	return columnData;
}

ostream & operator<<(std::ostream & os, Fact & Fact) {
	os << Fact.id;
	os << Fact.LEFT_PAREN;
	os << Fact.stringList;
	os << Fact.RIGHT_PAREN;
	os << Fact.PERIOD;
	return os;
}
