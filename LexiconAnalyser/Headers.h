#pragma once

using namespace std;

#include <set>
#include "ColumnColumnParameters.h"

class Headers
{
private:
	vector<string> headers;
public:
	Headers();
	~Headers();
	void addHeader(string newHeader);
	Headers modifyHeaders(vector<int> columnIndexes);
	vector<string> getHeaders();
	vector<int> getColumnsToMerge(Headers mergeHeader);
	vector<int> createMappingFor(Headers mergeHeader);
	set<ColumnColumnParameters> getHeaderMappings(Headers mergeHeader);
	Headers mergeWith(Headers mergeHeaders, vector<int> columnsToMerge);
	friend ostream& operator <<(ostream &os, Headers &Headers);
};

