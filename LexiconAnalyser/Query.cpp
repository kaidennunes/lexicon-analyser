/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Create Lexer class, which creates a vector of tokens from a input string
*/

#include "stdafx.h"
#include "Query.h"


Query::Query()
{
}

Query::Query(Predicate newPredicateList)
{
	predicate = newPredicateList;
	Q_MARK = '?';
}

Query::~Query()
{
}

Predicate Query::getPredicate()
{
	return predicate;
}

ostream & operator<<(std::ostream & os, Query & Query) {
	os << Query.predicate;
	os << Query.Q_MARK;
	return os;
}
