#include "stdafx.h"
#include "Headers.h"


Headers::Headers()
{
}

Headers::~Headers()
{
}

void Headers::addHeader(string newHeader)
{
	headers.push_back(newHeader);
}

Headers Headers::modifyHeaders(vector<int> columnIndexes)
{
	Headers newHeaders = Headers();
	for (unsigned int i = 0; i < columnIndexes.size(); i++)
	{
		newHeaders.addHeader(headers[columnIndexes[i]]);
	}
	return newHeaders;
}

vector<string> Headers::getHeaders()
{
	return headers;
}

vector<int> Headers::getColumnsToMerge(Headers mergeHeader)
{
	vector<int> columnsToMerge = vector<int>() ;
	for (unsigned int i=0;i<mergeHeader.headers.size();i++)
	{
		bool needsToBeMerged = true;
		for (unsigned int j = 0; j<headers.size(); j++)
		{
			if (headers[j] == mergeHeader.headers[i])
			{
				needsToBeMerged = false;
				break;
			}
		}
		if (needsToBeMerged)
		{
			columnsToMerge.push_back(i);
		}
	}
	return columnsToMerge;
}

vector<int> Headers::createMappingFor(Headers mergeHeader)
{
	vector<int> mapping = vector<int>();
	for (unsigned int i = 0; i<headers.size(); i++)
	{
		for (unsigned int j = 0; j<mergeHeader.headers.size(); j++)
		{
			if (headers[i] == mergeHeader.headers[j])
			{
				mapping.push_back(j);
				break;
			}
		}
	}
	return mapping;
}

set<ColumnColumnParameters> Headers::getHeaderMappings(Headers mergeHeader)
{
	set<ColumnColumnParameters> columnMappings = set<ColumnColumnParameters>();
	for (unsigned int i = 0; i<headers.size(); i++)
	{
		for (unsigned int j = 0; j < mergeHeader.headers.size(); j++)
		{
			if (headers[i] == mergeHeader.headers[j])
			{
				ColumnColumnParameters newColumnColumnParameters = ColumnColumnParameters(i, j);
				columnMappings.insert(newColumnColumnParameters);
				break;
			}
		}
	}
	return columnMappings;
}

Headers Headers::mergeWith(Headers mergeHeaders, vector<int> columnsToMerge)
{
	// Initialize the new header
	Headers newHeader = Headers();

	// Union the headers (the columns to merge are for the merging header, to be added into the first header)
	for (unsigned int i = 0; i<headers.size(); i++)
	{
		newHeader.addHeader(headers[i]);
	}
	for (unsigned int i=0;i<columnsToMerge.size();i++)
	{
		newHeader.addHeader(mergeHeaders.headers[columnsToMerge[i]]);
	}
	return newHeader;
}

ostream & operator<<(std::ostream & os, Headers & Headers)
{
	for (unsigned int i = 0; i<Headers.headers.size(); i++)
	{
		os << Headers.headers[i];
		if (Headers.headers.size() - 1 != i) os << ", ";
	}
	return os;
}