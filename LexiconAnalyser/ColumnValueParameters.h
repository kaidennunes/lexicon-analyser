#pragma once
#include "ColumnParameters.h"
class ColumnValueParameters :
	public ColumnParameters
{
private:
	string firstColumn;
	int secondColumn;
public:
	ColumnValueParameters();
	ColumnValueParameters(string newFirstColumn, int newSecondColumn);
	~ColumnValueParameters();
	string getFirstColumn();
	int getSecondColumn();
};

