#pragma once

using namespace std;

#include <set>
#include "ColumnColumnParameters.h"

class Row
{
private:
	vector<string> data;
public:
	Row();
	~Row();
	Row modifyRows(vector<int> columnIndexes);
	void addColumn(string newData);
	bool hasValueAtIndex(int index, string value);
	bool indexesHaveSameValue(int firstIndex, int secondIndex);
	void printRow(vector<string> headers);
	bool matchs(Row matchRow, set<ColumnColumnParameters> columnMappings);
	Row mergeWith(Row mergeRow, vector<int> columnsToMerge);
	Row rearrange(vector<int> mapping);
	friend ostream& operator <<(ostream &os, Row &Row);
	bool operator == (const Row& row) const {
		return data == row.data;
	}
	bool operator < (const Row& row) const {
		return data < row.data;
	}
	bool operator > (const Row& row) const {
		return data > row.data;
	}
};

