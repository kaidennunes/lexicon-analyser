#pragma once

using namespace std;

#include "Lexer.h"

class Id
{
private:
	string id;
public:
	Id();
	Id(Lexer *lexer);
	~Id();
	string getId();
	friend ostream& operator <<(ostream &os, Id &Id);
};

