/*
Kaiden Nunes, Section 3, kaiden@ranunes.com
Purpose: Defines token class
*/
#include "stdafx.h"
#include "Token.h"


Token::Token(string inputCharacters, string inputTokenType, unsigned int inputLineNumber)
{
	characters = inputCharacters;
	tokenType = inputTokenType;
	lineNumber = inputLineNumber;
}


Token::~Token()
{
}

string Token::getString()
{
	return characters;
}

string Token::getTokenType()
{
	return tokenType;
}

ostream & operator<<(ostream & os, Token & token)
{
	return os << "(" << token.tokenType << ",\"" + token.characters + "\"," << token.lineNumber << ')' << endl;
}
